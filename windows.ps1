$URL = "https://gitlab.com/schale/SillyTavern-Bootstrapper/-/raw/main/start.py"
$OUT = "./start.py"

if (!(&{ python --version 2>&1 >$NULL; $? })) {
    $PythonVersion = "3.11"
    $PythonDownloadUrl = "https://www.python.org/downloads/"

    Write-Host "Installing Python $PythonVersion"
    try {
        & winget install -e --id "Python.Python.$PythonVersion"
        # Refresh PATH environment variable to access python
        $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
    } catch [System.Management.Automation.CommandNotFoundException] {
        Write-Error "You don't have winget installed. Please download Python $PythonVersion manually from $PythonDownloadUrl"
        Start-Process $PythonDownloadUrl
        exit
    }
}

Invoke-WebRequest -Uri $URL -OutFile $OUT
& python $OUT