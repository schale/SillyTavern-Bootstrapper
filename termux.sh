#!/bin/bash

if ! command -v termux-setup-storage &> /dev/null
then
    echo "This script is for Termux Android application only - https://termux.dev"
    exit 1
fi

python_version=$(python -V 2>&1 | grep -Po '(?<=Python )(.+)')
if [[ -z "$python_version" ]]
then
    echo "Python not found, installing..."
    pkg update && pkg install python3 -y
fi

if ! command -v node &> /dev/null
then
    echo "Node.JS not found, installing..."
    pkg update && pkg install build-essential binutils nodejs -y
fi

url="https://gitlab.com/schale/SillyTavern-Bootstrapper/-/raw/main/start.py"
out="./start.py"

curl -o $out $url && python3 $out