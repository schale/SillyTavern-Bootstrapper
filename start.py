import urllib.request, urllib.error
import sys
import os, os.path
from pathlib import Path
import hashlib
import zipfile
import json
import shutil
import subprocess
import signal
import fnmatch

DATA_PATH = "./data-donotdelete/"
DATA_PRELOAD_MARKER_PATH = DATA_PATH + ".preload-installed"
DATA_DEV_MARKER_PATH = DATA_PATH + ".dev-mode-enabled"

ST_DOWNLOAD_URL = "https://gitlab.com/api/v4/projects/51251984/packages/generic/ci-release/1.0.0/dist.zip"
ST_DOWNLOAD_ARCHIVE_NAME = "dist.zip"
ST_PATH = "./sillytavern/"

# Windows only
NODE_DOWNLOAD_URL = "https://gitlab.com/api/v4/projects/51255246/packages/generic/node-win-x64/18.18.2/node-v18.18.2-win-x64.zip"
NODE_DOWNLOAD_ARCHIVE_NAME = "node-win-x64.zip"
NODE_PATH = "./nodejs/"
NODE_ARCHIVE_ROOT_FOLDER = "node-v18.18.2-win-x64/"

PRELOAD_FILE_PATH = "preload.json"
PRELOAD_DOWNLOAD_ARCHIVE_NAME = "preload.zip"

if not os.path.isdir(DATA_PATH):
    os.mkdir(DATA_PATH)

def is_dev_mode():
    return os.path.isfile(DATA_DEV_MARKER_PATH)

preload = False
preload_manifest = None
if os.path.isfile(PRELOAD_FILE_PATH):
    print("Content preload manifest has been found")
    with open(PRELOAD_FILE_PATH, "r") as f:
        preload = True
        preload_manifest = json.load(f)

def read_etag(path):
    etag_file = path + ".etag"
    if os.path.isfile(etag_file):
        with open(etag_file, "r") as f:
            return f.read()
    return ""
def write_etag(path, etag):
    etag_file = path + ".etag"
    with open(etag_file, "w") as f:
        f.write(etag)
def delete_etag(path):
    etag_file = path + ".etag"
    if os.path.isfile(etag_file):
        os.unlink(etag_file)

def download_file(url, path):
    headers = {
        "If-None-Match": read_etag(path)
    } if os.path.exists(path) else {}
    if os.path.exists(path) and is_dev_mode():
        print(f"{path} dev mode enabled, file always changed")
        return os.path.exists(path + ".dev-changed")
    request = urllib.request.Request(url, None, headers=headers)
    try:
        with urllib.request.urlopen(request) as response:
            if ("If-None-Match" in headers and response.getheader("etag", None) == headers["If-None-Match"]):
                print(f"{path} is not modified, not downloading")
                return False
            elif response.status == 200:
                print(f"Downloading {url} to {path}")
                total_length = response.getheader("content-length", None)
                etag = response.getheader("etag", None)
                delete_etag(path)
                with open(path, "wb") as f:
                    if total_length is None: # no content length header
                        f.write(response.read())
                    else:
                        dl = 0
                        total_length = int(total_length)
                        while data := response.read(4096):
                            dl += len(data)
                            f.write(data)
                            done = int(50 * dl / total_length)
                            sys.stdout.write("\r[%s%s] %.2f%%" % ('=' * done, ' ' * (50 - done), round(100 * (dl / total_length), 2)))    
                            sys.stdout.flush()
                        sys.stdout.write("\n")
                if etag:
                    write_etag(path, etag)
                print(f"{path} downloaded")
                return True
            else:
                print(f"Invalid file download status code {response.status} from {url} to {path}")
                delete_etag(path)
                return False
    except urllib.error.HTTPError as e:
        if e.status == 304:
            print(f"{path} is not modified, not downloading")
        else:
            print(f"Invalid file download status code {response.status} from {url} to {path}")
            delete_etag(path)
        return False
    except urllib.error.URLError as e:
        print(f"Connection to {url} failed:", e)
        delete_etag(path)
        exit(1)

ST_DOWNLOAD_ARCHIVE_PATH = DATA_PATH + ST_DOWNLOAD_ARCHIVE_NAME
st_package_modified = download_file(ST_DOWNLOAD_URL, ST_DOWNLOAD_ARCHIVE_PATH)

clean_installation = False
if not os.path.isdir(ST_PATH):
    os.mkdir(ST_PATH)
    print("Clean installation")
    clean_installation = True
if not clean_installation and os.path.isdir(ST_PATH) and not os.path.isfile(ST_PATH + "package.json"):
    print("Broken installation")
    clean_installation = True
elif st_package_modified:
    print("A SillyTavern update has been detected and will be installed")

if clean_installation:
    if os.path.isfile(DATA_PRELOAD_MARKER_PATH):
        os.unlink(DATA_PRELOAD_MARKER_PATH)

if clean_installation or st_package_modified:
    print(f"Extracting SillyTavern to {ST_PATH}")
    with zipfile.ZipFile(ST_DOWNLOAD_ARCHIVE_PATH, "r") as z:
        z.extractall(ST_PATH)

preload_added = preload and not os.path.isfile(DATA_PRELOAD_MARKER_PATH)

if preload:
    PRELOAD_DOWNLOAD_ARCHIVE_PATH = DATA_PATH + PRELOAD_DOWNLOAD_ARCHIVE_NAME
    PRELOAD_CHANGES_LOG_PATH = ST_PATH + "PRELOAD_CHANGES.md"
    if "PreloadFilesUrl" not in preload_manifest:
        print("ERROR: Preload manifest exists but does not contain content download URL")
        exit(1)
    preload_package_modified = download_file(preload_manifest["PreloadFilesUrl"], PRELOAD_DOWNLOAD_ARCHIVE_PATH)
    if preload_package_modified or clean_installation or preload_added:
        if os.path.isfile(PRELOAD_CHANGES_LOG_PATH):
            os.unlink(PRELOAD_CHANGES_LOG_PATH)
        with zipfile.ZipFile(PRELOAD_DOWNLOAD_ARCHIVE_PATH, "r") as z:
            try:
                data = z.read(PRELOAD_FILE_PATH)
                preload_manifest = json.loads(data)
                print("Manifest was found in the preload package, using package manifest")
            except KeyError:
                print("Manifest not found in preload package, using local manifest")
            if "DeleteSTDefaultFiles" in preload_manifest:
                for file in preload_manifest["DeleteSTDefaultFiles"]:
                    if file.startswith("."):
                        print(f"Skipping invalid filename from deletion {file}")
                        continue
                    full_path = ST_PATH + file
                    if not os.path.exists(full_path):
                        print(f"Skipping already nonexistent default file {file}")
                        continue
                    print(f"Deleting default file {file}")
                    if os.path.isfile(full_path):
                        os.remove(full_path)
                    else:
                        shutil.rmtree(full_path)
            print(f"Extracting preload content to {ST_PATH} - ", end="")
            if "UpdateFilesGlobs" in preload_manifest and (not clean_installation and not preload_added):
                print("only update files")
                for file in z.namelist():
                    dest = ST_PATH + file
                    for glob in preload_manifest["UpdateFilesGlobs"]:
                        if not os.path.exists(dest) or fnmatch.fnmatch(file, glob):
                            print(f"- {file}")
                            if file.endswith("/"):
                                if not os.path.isdir(dest):
                                    os.mkdir(dest)
                            else:
                                z.extract(file, ST_PATH)
                            break
            else:
                print("all package files")
                z.extractall(ST_PATH)
        if os.path.isfile(PRELOAD_CHANGES_LOG_PATH):
            with open(PRELOAD_CHANGES_LOG_PATH, "r") as f:
                print("\nContent changes:\n" + f.read() + "\n")
        with open(DATA_PRELOAD_MARKER_PATH, "w") as f:
            f.write("created")

os.chdir(ST_PATH)

for file in Path("./default/").glob("*"):
    file_to_path_map = {
        "user.css": "public/css/user.css",
        "config.yaml": "config.yaml"
    }
    filename = file.name
    dest = file_to_path_map[filename] if filename in file_to_path_map else filename
    if (not os.path.exists(dest) or clean_installation or preload_added) and filename in file_to_path_map:
        print("Copying", file, "to", dest)
        if os.path.isdir(file):
            if os.path.isdir(dest):
                shutil.rmtree(dest)
            shutil.copytree(file, dest)
        else:
            shutil.copy(file, dest)

sillytavern_executable = None
if sys.platform == "win32":
    os.chdir("..")
    NODE_DOWNLOAD_ARCHIVE_PATH = DATA_PATH + NODE_DOWNLOAD_ARCHIVE_NAME
    node_package_modified = download_file(NODE_DOWNLOAD_URL, NODE_DOWNLOAD_ARCHIVE_PATH)

    if node_package_modified or not os.path.isdir(NODE_PATH) or not os.path.isfile(NODE_PATH + "node.exe"):
        print("Installing Node.js")
        if os.path.isdir(NODE_PATH):
            shutil.rmtree(NODE_PATH)
        os.mkdir(NODE_PATH)
        with zipfile.ZipFile(NODE_DOWNLOAD_ARCHIVE_PATH, "r") as z:
            for file in z.namelist():
                if file.startswith(NODE_ARCHIVE_ROOT_FOLDER) and file != NODE_ARCHIVE_ROOT_FOLDER:
                    new_path = NODE_PATH + file[len(NODE_ARCHIVE_ROOT_FOLDER):]
                    if file[-1] == "/":
                        os.mkdir(new_path)
                    else:
                        with z.open(file, "r") as zf:
                            with open(new_path, "wb") as ff:
                                ff.write(zf.read())
    os.environ["PATH"] = os.path.abspath(NODE_PATH) + os.pathsep + os.environ["PATH"]
    os.chdir(ST_PATH)

    subprocess.call(["..\\nodejs\\npm.cmd", "install", "--no-audit"])

    sillytavern_executable = [
        "../nodejs/node.exe",
        "server.js"
    ]
elif sys.platform == "linux" or sys.platform == "linux2":
    sillytavern_executable = "./sillytavern-linux-manual.sh"
    def make_executable(path):
        mode = os.stat(path).st_mode
        mode |= (mode & 0o444) >> 2    # copy R bits to X
        os.chmod(path, mode)
    make_executable(sillytavern_executable)
elif sys.platform == "darwin":
    print("macOS is not supported")
    exit(1)

print(f"Launching SillyTavern - {sillytavern_executable}")
p = None
try:
    p = subprocess.Popen(sillytavern_executable)
    p.wait()
except KeyboardInterrupt:
    try:
        p.send_signal(signal.SIGINT)
    except subprocess.TimeoutExpired:
        p.kill()
print("Done, exiting")

os.chdir("..")