# Legacy, kept as reference, use the Python version instead

$CONST_ST_DIST_ZIP_URL = "https://gitlab.com/api/v4/projects/51251984/packages/generic/ci-release/1.0.0/dist.zip";
$CONST_ST_DIST_ZIP_PATH = "./dist-keepthisfile.zip"
$CONST_ST_UNPACK_PATH = "./sillytavern/"
$CONST_PRELOAD_CONTENT_DATA_PATH = "./preload.json"
$CONST_PRELOAD_CONTENT_FILES_ZIP_PATH = "./preload-keepthisfile.zip"

Function Download-File {
    Param (
        [Parameter(Mandatory = $True)] [System.Uri]$uri,
        [Parameter(Mandatory = $True )] [string]$FilePath
    )

    # Make sure the destination directory exists
    # System.IO.FileInfo works even if the file/dir doesn't exist, which is better then get-item which requires the file to exist
    If (! ( Test-Path ([System.IO.FileInfo]$FilePath).DirectoryName ) ) { [void](New-Item ([System.IO.FileInfo]$FilePath).DirectoryName -force -type directory) }

    try {
        $headers = @{}

        if (Test-Path $FilePath) {
            $hash = (Get-FileHash $FilePath -Algorithm MD5).Hash.ToLower()
            $headers['If-None-Match'] = "`"$hash`""
        }

        $ProgressPreference = 'SilentlyContinue'
        Invoke-WebRequest -Uri $uri -Headers $headers -OutFile $FilePath
        $ProgressPreference = 'Continue'

        return $true
    }
    catch [System.Net.WebException] {
        # Check for a 304
        if ($_.Exception.Response.StatusCode -eq [System.Net.HttpStatusCode]::NotModified) {
            Write-Host "$FilePath not modified, not downloading..."
        }
        else {
            # Unexpected error
            $Status = $_.Exception.Response.StatusCode
            $msg = $_.Exception
            Write-Host "! Error dowloading $FilePath, Status code: $Status - $msg"
        }

        return $false
    }
}

$PreloadData = @{}
if (Test-Path $CONST_PRELOAD_CONTENT_DATA_PATH) {
    Write-Host "Content preload manifest has been found"
    $PreloadData = (Get-Content $CONST_PRELOAD_CONTENT_DATA_PATH | ConvertFrom-Json)
}

Write-Host "Downloading SillyTavern self-packaged distribution package as $CONST_ST_DIST_ZIP_PATH (do not remove this file after installation)";
$STPackageUpdated = Download-File $CONST_ST_DIST_ZIP_URL $CONST_ST_DIST_ZIP_PATH;
if ($STPackageUpdated) {
    Write-Host "A SillyTavern update has been detected and will be applied";
}

if (!(Test-Path $CONST_ST_UNPACK_PATH) -or $STPackageUpdated) {
    Write-Host "Extracting SillyTavern to $CONST_ST_UNPACK_PATH";
    Expand-Archive $CONST_ST_DIST_ZIP_PATH $CONST_ST_UNPACK_PATH -Force;
}

if ($PreloadData.PreloadFilesUrl) {
    $url = $PreloadData.PreloadFilesUrl
    Write-Host "Downloading preload content from $url as $CONST_PRELOAD_CONTENT_FILES_ZIP_PATH (do not remove this file after installation as well)"

    $PreloadPackageModified = Download-File $url $CONST_PRELOAD_CONTENT_FILES_ZIP_PATH;
    if ($PreloadPackageModified) {
        Write-Host "A preload content update has been detected and will be applied";

        if ($PreloadData.DeleteSTDefaultContent) {
            Write-Host "Content preload manifest specified removing default content";
            Remove-Item -ErrorAction Ignore -Recurse "$CONST_ST_UNPACK_PATH/default/content";
        }

        Expand-Archive $CONST_PRELOAD_CONTENT_FILES_ZIP_PATH $CONST_ST_UNPACK_PATH;
    }
}

Set-Location $CONST_ST_UNPACK_PATH

if (!(Test-Path "./default")) {
    New-Item "./default" -ItemType Directory
}
Get-ChildItem "./default" -File |
ForEach-Object {
    if (!(Test-Path $_)) {
        Write-Host "Writing default file $_"
        Copy-Item -Recurse $_.FullName $_
    }
}

$STBinaryFileName = $null
if ($IsLinux) {
    $STBinaryFileName = "./sillytavern-linux"
    chmod +x $STBinaryFileName
}
elseif ($IsMacOS) {
    $STBinaryFileName = "./sillytavern-macos"
    Write-Host "Running on macOS, this will probably fail (no ARM/M1 builds and code signing is a mess). Just follow the official guide."
}
elseif ($IsWindows -or ([System.Environment]::OSVersion.Platform.ToString() -eq 'Win32NT')) {
    $STBinaryFileName = "./sillytavern-win.exe"
} else {
    Write-Error "Unknown platform!"
    exit
}

Write-Host "Starting SillyTavern - $STBinaryFileName"
Start-Process -Wait $STBinaryFileName
Write-Host "Done, exiting"

Set-Location ..